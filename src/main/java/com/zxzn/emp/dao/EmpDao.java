package com.zxzn.emp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxzn.emp.entity.EmpEntity;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface EmpDao extends BaseMapper<EmpEntity> {


}
