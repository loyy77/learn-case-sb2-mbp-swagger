package com.zxzn.emp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxzn.emp.bo.EmpQueryBo;
import com.zxzn.emp.entity.EmpEntity;
import com.zxzn.emp.service.EmpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author apple
 */
@Api(value = "用户操作",tags = "用户操作")
@RestController
@RequestMapping("/emp")
public class EmpController {
    
    @Autowired
    private EmpService empService;
    
    @ApiOperation("列表分页")
    @GetMapping("/page")
    public IPage list(@ModelAttribute EmpQueryBo bo) {
        return empService.page(new Page<>(bo.getPageNo(), bo.getPageSize()), new QueryWrapper<>(EmpEntity.from(bo)));
    }
    
    @ApiOperation("修改用户")
    @PostMapping("/update")
    public Boolean update(EmpEntity empEntity) {
        return empService.updateById(empEntity);
    }
    
    @ApiOperation("添加用户")
    @PutMapping("/add")
    public Boolean add(EmpEntity empEntity) {
        return empService.save(empEntity);
    }
    
    @ApiOperation("删除用户")
    @DeleteMapping("/delete")
    public Boolean delete(int id) {
        return empService.removeById(id);
    }
}
