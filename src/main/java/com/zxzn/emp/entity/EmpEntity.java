package com.zxzn.emp.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zxzn.emp.bo.EmpQueryBo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("t_emp")
@Builder
public class EmpEntity {
    
    private Integer id;
    private String name;
    private Integer age;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date hiredate;
    private Integer deptno;
    
    public static EmpEntity from(EmpQueryBo bo) {
        return EmpEntity.builder()
                .age(bo.getAge())
                .deptno(bo.getDeptno())
                .hiredate(bo.getHiredate())
                .name(bo.getName())
                .build();
    }
}
