package com.zxzn.emp.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxzn.emp.dao.EmpDao;
import com.zxzn.emp.entity.EmpEntity;
import com.zxzn.emp.service.EmpService;
import org.springframework.stereotype.Service;



@Service
public class EmpServiceImpl extends ServiceImpl<EmpDao, EmpEntity> implements EmpService {


}
