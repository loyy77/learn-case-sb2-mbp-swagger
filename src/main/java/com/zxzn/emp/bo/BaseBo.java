package com.zxzn.emp.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author LiShixi 2021/8/20 9:14 下午
 */
@Data
public class BaseBo {
    @ApiModelProperty("页号")
    int pageNo;
    @ApiModelProperty("每页大小")
    int pageSize = 10;
}
