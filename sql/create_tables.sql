create table if not exists t_emp
(
    id serial
        constraint t_emp_pk
            primary key,
    name varchar(20),
    age integer,
    hiredate timestamp,
    deptno integer
);

alter table t_emp owner to postgres;

